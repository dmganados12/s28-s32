// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Schema
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Name field is Required']
		},
		description: {
			type: String,
			required: [true, 'Description field is Required']
		},
		price: {
			type: Number,
			required: [true, 'Price field is Required']
		},
		isActive: {
			type: Boolean,
			default: true 
		},
		createdOn: {
			type: Date,
			default: new Date() 
		},
		enrollees: [
			{
				userID: {
					type: String,
					required: [true, "User's ID is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});

// [SECTION] Model
	module.exports = mongoose.model("Course", courseSchema)