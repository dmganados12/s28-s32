// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/courses');
	const auth = require('../auth');

// [SECTION] Routing Components
	const route = exp.Router();

// [SECTION]-[POST] Routes (Admin Only)
	route.post('/', auth.verify,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body,			
		};
		if (isAdmin) {
			controller.addCourse(data).then(outcome => {
				res.send(outcome)
			});
		} else {
			res.send('User Unauthorized to Proceed!')
		}		
	});

// [SECTION]-[GET] Routes
	// Retrieve All Courses
	route.get('/all', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		isAdmin ? controller.getAllCourse().then(outcome => res.send(outcome))
		: res.send('Unauthorized user');		
	});

	// Retrieve Active Courses
	route.get('/', (req, res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome)
		});
	});

	// Retrieve Single Course
	route.get('/:id', (req, res) => {
		let data = req.params.id;
		controller.getCourse(data).then(result => {
			res.send(result)
		});
	});

// [SECTION]-[PUT] Routes Administrator
	route.put('/:courseId', auth.verify,(req, res) => {
		let params = req.params;
		let body = req.body;
		if (auth.decode(req.headers.authorization).isAdmin) {
			controller.updateCourse(params, body).then(outcome => {
				res.send(outcome)
			});			
		} else {
			res.send('User Unauthorized')
		}	
	});

	route.put('/:courseId/archive', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? 
			controller.archiveCourse(params).then(result => 
				res.send(result))
			:
			res.send('Unauthorized user');
	});

// [SECTION]-[DEL] Routes
	route.delete('/:courseId', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.courseId
		isAdmin ? controller.deleteCourse(id).then(outcome => res.send(outcome))
		: res.send('Unauthorized user');
	});	

// [SECTION] Export Route System 
	module.exports = route;