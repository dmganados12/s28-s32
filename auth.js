// [SECTION] Dependencies and Modules
	const jwt = require('jsonwebtoken');
	const dotenv = require('dotenv');

// [SECTION] Environment Variable Setup
	dotenv.config();
	let secret = process.env.SECRET;

// [SECTION] Funtionalities
	module.exports.createAccessToken = (authUser) => {
		let userInfo = {
			id:authUser._id,
			email:authUser.email,
			isAdmin:authUser.isAdmin
		};
		return jwt.sign(userInfo, secret, {});
	};

	module.exports.verify = (req, res, next) => {
		let token = req.headers.authorization;
		if (typeof token !== 'undefined') {
			token = token.slice(7, token.length);
			jwt.verify(token, secret, (err, payload) => {
				if (err) {
					return res.send({auth: 'Authorization Failed'})
				} else {
					next();
				}
			});
		} else {
			return res.send({auth: "Authorization Failed, Check Token"});
			
		};
	};

	module.exports.decode = (acessToken) => {
		if (typeof acessToken !== 'undefined') {
			acessToken = acessToken.slice(7, acessToken.length);
			return jwt.verify(acessToken, secret, (err, verified) => {
				if (err) {
					return null;
				} else {
					return jwt.decode(acessToken, {complete: true}).payload;
				}
			});
		} else {
			return null;
		}		
	};