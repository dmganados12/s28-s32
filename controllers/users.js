// [SECTION] Dependecies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

// [SECTION] Functionalities-[Create]
	module.exports.registerUser = (data) => {
		let fName =	data.firstName;
		let lName =	data.lastName;
		let email =	data.email;
		let passW =	data.password;
		let mobil =	data.mobileNo;
		let newUSer = new User ({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10),
			mobileNo: mobil 
		});
		return newUSer.save().then((user, err) => {				
			if (user) {
				return user;
			} else {
				return false;
			}
		});
	};

	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {			
			if (result.length > 0) {
				return 'Email already exists';
			} else {
				return 'Email is still  available'
			};
		});
	};

	module.exports.loginUser = (reqBody) => {		
		return User.findOne({email: reqBody.email}).then(result => {
			let uPassW = reqBody.password;
			if (result === null) {
				return false;
			} else {
				const isMatched = bcrypt.compareSync(uPassW, result.password);				
				if (isMatched) {
					let userData = result.toObject();
					return {accessToken: auth.createAccessToken(userData)};
				} else {
					return false;
				}

			};
		});
	};

	module.exports.enroll = async (data) => {				
					let id = data.userId;
					let course = data.courseId;						
					let isUserUpdated = await User.findById(id).then(user => {						
						let userData = user.enrollments;
						let findDups = userData.find((duplicate) => {
							return (duplicate.courseId === course)	
						});

						if (findDups) {
							return false;
						} else {
							user.enrollments.push({courseId: course});
							return user.save().then((save, error) => {
							    if (error) {
							        return false;
							    } else {
							        return true; 
							    }
							});
						}									

					});
	

					let isCourseUpdated = await Course.findById(course).then(course => {				
						course.enrollees.push({userID: id});
						return course.save().then((save, err) => {
							if (err) {
								return false;
							} else {
								return true;
							};
						});	
						
					});
					if (isUserUpdated && isCourseUpdated) {
						return 'Course is successfully added'; 
					} else if (!isUserUpdated){
						return 'Failed to add course. Student is already enrolled.'
					} else {
						return 'Enrollment Failed, Go to Registrar';
					};					
	};

// [SECTION] Functionalities-[Retrieve]
	module.exports.getProfrile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};
	

// [SECTION] Functionalities-[Update]
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return 'Updates Failed to implement';
			}
		})
	};

	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return true; 
			} else {
				return 'Failed to update user';
			};
		});
	};

